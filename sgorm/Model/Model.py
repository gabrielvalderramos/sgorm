import DataType


class Entity(object):
    def __init__(self):
        self._to_commit = {}

        self.type = DataType.Field("type", None)
        self.id = DataType.Field("id", None)
