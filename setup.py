import setuptools

with open("README.md", "r") as fh:
    long_description = fh.read()

with open("requirements.txt") as rf:
    requirements = rf.read().splitlines()

setuptools.setup(
    name="sgorm",
    version="0.0.1",
    author="Gabriel Valderramos",
    author_email="gabrielvalderramos@gmail.com",
    description="Shotgun ORM Library",
    long_description=long_description,
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/gabrielvalderramos/sgorm",
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 2.7",
        "License :: OSI Approved :: MIT License",
        "Operating System :: OS Independent",
    ],
    python_requires='>=2.7'
)